﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polimorfismo
{
    class Program
    {
        static void Main(string[] args)
        {
            Pessoa pessoa = new Pessoa();
            pessoa.gravarDados();

            PessoaFisica pf = new PessoaFisica();
            pf.gravarDados();

            PessoaJuridica pj = new PessoaJuridica();
            pj.gravarDados();

            Console.ReadKey();
        }
    }
}
