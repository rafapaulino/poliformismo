﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polimorfismo
{
    class PessoaFisica : Pessoa
    {
        public override void gravarDados()
        {
            Console.WriteLine("Gravando dados pessoa física");
        }
    }
}
